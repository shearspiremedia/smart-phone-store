import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent {
  items;
  checkoutForm;
  submitted = false;
  customerData;
  constructor(
    private cartService: CartService,
    private formBuilder: FormBuilder,
  ) {
    this.items = this.cartService.getItems();
    /*
    During checkout, the app will prompt the user for a name and address.
    So that you can gather that information later, set the checkoutForm property
    with a form model containing name and address fields, using the FormBuilder#group() method.
    */
    this.checkoutForm = this.formBuilder.group({
      name: '',
      address: ''
    });
  }
  onSubmit(customerData) {
    // Process checkout data here
    console.warn('Your order has been submitted', customerData);
    this.customerData = customerData;
    this.items = this.cartService.clearCart();
    this.checkoutForm.reset();
    this.submitted = true;
  }
}
