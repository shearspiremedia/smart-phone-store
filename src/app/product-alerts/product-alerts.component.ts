import { Component, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-product-alerts',
  templateUrl: './product-alerts.component.html',
  styleUrls: ['./product-alerts.component.css']
})

/*
The @Input() decorator indicates that the property value passes in from the component's parent, the product list component.
*/
export class ProductAlertsComponent {
  @Input() product;
  @Output() notify = new EventEmitter();
}
