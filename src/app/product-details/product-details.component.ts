import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { products } from '../products';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  product;
  constructor(
    private route: ActivatedRoute,
    private cartService: CartService,
  ) {  }
  addToCart(product){
    window.alert(`Your product (${product.name}) has been added to the cart!`);
    this.cartService.addToCart(product);
  }
  /*
  The ActivatedRoute is specific to each routed component loaded by the Angular Router.
  It contains information about the route, its parameters, and additional data associated with the route.
  */
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.product = products[+params.get('productId')];
    });

  }
}
