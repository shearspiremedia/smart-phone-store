import { Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
/*
The Input grabs the appName property from the parent module (app.component) of this module.
Within the html template of the parent module, appName is passed in as a property.
*/
export class TopBarComponent implements OnInit {
  @Input() appName: string;
  route: string;
  constructor(location: Location, router: Router) {
    router.events.subscribe(val => {
      if (location.path() !== ''){
        this.route = location.path();
      } else {
        this.route = 'Home';
      }
    });
  }
  ngOnInit() {
  }

}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
